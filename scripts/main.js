requirejs.config({
    paths: {
        'modules'    : '../modules'
    }
});


define(['modules/moduleA'], function(A){
    A.init();
});
