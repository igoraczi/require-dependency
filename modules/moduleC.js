define([], function() {
    var A = function() {
        return require('modules/moduleA');
    };

    document.getElementById('element').addEventListener('click', function(e) {
        A().init();
    });
});
